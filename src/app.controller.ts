import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { getRepos } from '@mrvyldr/js-npm-registry';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('/merve')
  getHello(): string {
    console.log('hello controller');
    getRepos({
      username = 'mrvyldr',
      page = 1,
      per_page = 30,
    });
    return this.appService.getHello();
  }

  @Get('/exampleapi')
  getExampleApi(): string {
    console.log('exampleapi controller');
    return this.appService.getExampleApi();
  }
}
